import './App.css';

import React, { Component } from 'react';
import Websocket from 'react-websocket';
import Time from './Time';
import Queue from './Queue';

function convertPatient(patient) {
  const start = new Date(patient.start);
  const end = new Date(start.getTime() + patient.duration * 60000);
  const hours1 = ("0" + start.getHours()).slice(-2);
  const hours2 = ("0" + end.getHours()).slice(-2);
  const minutes1 = ("0" + start.getMinutes()).slice(-2);
  const minutes2 = ("0" + end.getMinutes()).slice(-2);
  const roomOrTime = patient.room ? {room: patient.room} : {hours1, minutes1, hours2, minutes2};
  return Object.assign({}, {number: patient.queueNumber, category: patient.queueNumber[0]}, roomOrTime);
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      patients: []
    };
    fetch('//localhost:3000/api/v1/patients')
      .then(response => {
        return response.json();
      })
      .then(patients => {
        this.setState({patients: patients.map(convertPatient)});
      });
  }

  onMessage(msg) {
    const message = JSON.parse(msg);
    if (message.type === 'new_patient') {
      const patients = message.patients.map(convertPatient);
      this.setState({patients});
    }
  }

  render() {
    return (
      <div className="app">
        <header className="app__header">
          <h1 className="app__title">ER Queue</h1>
          <div className="app__time"><Time current big /></div>
        </header>
        <div className="app__queue">
          <Queue>
            {this.state.patients}
          </Queue>
        </div>
        <Websocket url='ws://localhost:3000/api/v1/' onMessage={this.onMessage.bind(this)}/>
      </div>
    );
  }
}

// [
//   {number: "A365", category: "A", room: "04"},
//   {number: "B004", category: "B", room: "01"},
//   {number: "C012", category: "C", room: "34"},
//   {number: "A366", category: "A", hours1: "16", minutes1: "34", hours2: "16", minutes2: "49"},
//   {number: "B005", category: "B", hours1: "16", minutes1: "32", hours2: "16", minutes2: "42"},
//   {number: "A367", category: "A", hours1: "16", minutes1: "44", hours2: "16", minutes2: "59"},
//   {number: "A368", category: "A", hours1: "16", minutes1: "54", hours2: "17", minutes2: "09"},
//   {number: "A369", category: "A", hours1: "17", minutes1: "04", hours2: "17", minutes2: "19"},
//   {number: "C013", category: "C", hours1: "17", minutes1: "20", hours2: "18", minutes2: "00"}
// ]

export default App;
