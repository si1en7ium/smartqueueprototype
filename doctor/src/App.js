import React, { Component } from 'react';
import Websocket from 'react-websocket';
import './App.css';
import Time from './Time';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      age: '',
      symptoms: '',
      id: '',
      havePatient: false,
      patients: []
    }
  }

  onMessage(msg) {
    const message = JSON.parse(msg);
    if (message.type === "new_patient") {
      const patients = message.patients.filter(p => p.active && p.doctor === this.props.doctor);
      this.setState({patients});
    }
  }

  donePatient() {
    fetch('http://localhost:3000/api/v1/patients/done', {
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify({id: this.state.id, doctor: this.props.doctor})
    }).then(() => {
      // whatever
    });
    this.setState({
      havePatient: false,
      name: '',
      age: '',
      symptoms: '',
      id: ''
    });
  }

  nextPatient() {
    const patient = this.state.patients[0];
    console.log(patient);
    this.setState({
      havePatient: true,
      name: patient.name,
      symptoms: patient.symptoms,
      age: patient.age,
      id: patient.id
    });
    fetch('http://localhost:3000/api/v1/patients/receive', {
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify({id: patient.id, doctor: this.props.doctor})
    }).then(() => {
      // whatever
    });
  }

  renderPatient() {
    return (
        <div className="col1 patient">
          <h2 className="patient__header">CURRENT PATIENT INFORMATION</h2>
          <label className="patient__label">Name:</label>
          <span className="patient__text patient__text_name">{this.state.name}</span>
          <label className="patient__label">Age:</label>
          <span className="patient__text patient__text_age">{this.state.age}</span>
          <label className="patient__label">Symptoms:</label>
          <span className="patient__text patient__text_symptoms">{this.state.symptoms}</span>
          <button className="patient__button" type="button" onClick={this.donePatient.bind(this)}>Done with current patient</button>
        </div>
    );
  }

  renderNoPatient() {
    const nextDisabled = this.state.patients.length === 0;
    return (
      <div className="patient patient_empty">
        <button className="patient__button" type="button" onClick={this.nextPatient.bind(this)} disabled={nextDisabled}>Next patient</button>
      </div>
    )
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Doctor X</h1>
          <div className="App-time"><Time current big /></div>
        </header>
        {this.state.havePatient ? this.renderPatient() : this.renderNoPatient()}
        <Websocket url='ws://localhost:3000/api/v1/' onMessage={this.onMessage.bind(this)}/>
      </div>
    );
  }
}

export default App;

//Two buttons (patient comes in, patient leaves)
//name, age and symptoms

