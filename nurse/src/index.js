import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import store from "./store";
import show from "./show";
import Nurse from './Nurse';
import './index.css';



const rootEl = document.getElementById("root");

ReactDOM.render(
	<Provider store={store}>
		<Nurse onSubmit={show} />
	</Provider>,
	rootEl
);