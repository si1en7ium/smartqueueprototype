const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export default (async function showResults(values) {
  await sleep(500); // simulate server latency
  fetch('http://localhost:3000/api/v1/patients', {
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify(values)
}).then(() => {
  console.log(`You submitted:\n\n${JSON.stringify(values, null, 2)}`);
});
});