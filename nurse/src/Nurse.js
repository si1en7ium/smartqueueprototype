import './Nurse.css';
import Time from './Time';
import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';

class Nurse extends Component {
    render() {
    	const {handleSubmit, pristine, reset, submitting} = this.props;
        return (
        	<div className="nurse">
	        	<header className="nurse_header">
			      <h1 className="nurse_title">Patient information</h1>
			      <div className="nurse_time"><Time current big /></div>
			    </header>

			    <form onSubmit={handleSubmit} className="nurse-form">
				      	<div className="input-field nurse-form__text nurse-form__text--name">
							<Field
								id="name-edit"
								name="name"
								component="input"
								type="text"
							/>
							<label htmlFor="name-edit">Name</label>
					    </div>
					    <div className="input-field nurse-form__text nurse-form__text--ssn">
							<Field
							    id="ssn-edit"
								name="ssn"
								component="input"
								type="text"
							/>
							<label htmlFor="ssn-edit">SSN</label>
					    </div>
					    <div className="input-field nurse-form__text nurse-form__text--age">
							<Field
								id="age-edit"
								name="age"
								component="input"
								type="text"
							/>
							<label htmlFor="age-edit">Age</label>
					    </div>
					    <div className="input-field nurse-form__text nurse-form__text--phone">
							<Field
								id="phone-edit"
								name="phone"
								component="input"
								type="text"
							/>
							<label htmlFor="phone-edit">Phone</label>
					    </div>
					    <div className="input-field nurse-form__text nurse-form__text--address">
							<Field
								id="address-edit"
								name="address"
								component="input"
								type="text"
							/>
							<label htmlFor="address-edit">Address</label>
					    </div>
						<div className="input-field nurse-form__text nurse-form__text--duration">
							<Field
								id="duration-edit"
								name="duration"
								component="input"
								type="text"
							/>
							<label htmlFor="duration-edit">Time estimate</label>
						</div>
					    <div className="input-field nurse-form__text nurse-form__text--symptoms">
							<Field
								id="symptoms-edit"
								name="symptoms"
								component="textarea"
								className="materialize-textarea"
							/>
							<label htmlFor="symptoms-edit">Symptoms</label>
					    </div>
						<div className="nurse-form__select nurse-form__select--doctor">
							<label>Doctor</label>
							<Field
								className="browser-default"
								name="doctor"
								component="select"
							>
								<option value="doctor1">Doctor 1</option>
								<option value="doctor2">Doctor 2</option>
								<option value="doctor3">Doctor 3</option>
								<option value="doctor4">Doctor 4</option>
						    </Field>
						</div>
						<div className="nurse-form__radio nurse-form__radio--priority radio">
							<label className="radio__label">Priority</label>
							<p className="radio__button radio__button--a">
								<Field
									id="edit-priority-a"
									name="priority"
									component="input"
									type="radio"
									value="A"
								/>
								<label htmlFor="edit-priority-a">Red</label>
							</p>
							<p className="radio__button radio__button--b">
								<Field
									id="edit-priority-b"
									name="priority"
									component="input"
									type="radio"
									value="B"
								/>
								<label htmlFor="edit-priority-b">Yellow</label>
							</p>
							<p className="radio__button radio__button--c">
								<Field
									id="edit-priority-c"
									name="priority"
									component="input"
									type="radio"
									value="C"
								/>
								<label htmlFor="edit-priority-c">Green</label>
							</p>
							<p className="radio__button radio__button--d">
								<Field
									id="edit-priority-d"
									name="priority"
									component="input"
									type="radio"
									value="D"
								/>
								<label htmlFor="edit-priority-d">Blue</label>
							</p>
						</div>
						<button type="submit" className="nurse-form__submit">Admit patient</button>
	    		</form>
	    	</div>
    	);
	}
}


export default reduxForm({
  form: 'nurse', // a unique identifier for this form
})(Nurse);















