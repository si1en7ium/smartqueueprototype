const express = require('express');

const app = express();
const port = process.env.PORT || 3000;
const expressWs = require('express-ws')(app);
const wss = expressWs.getWss('/');
const api = express.Router();

const patients = [];
let lastID = 0;

const doctors = {
    doctor1: {
        room: "1024",
        queueNumber: 1
    },
    doctor2: {
        room: "2042",
        queueNumber: 2
    },
    doctor3: {
        room: "1013",
        queueNumber: 3
    },
    doctor4: {
        room: "0420",
        queueNumber: 4
    }
};

function addPatient(patient) {
    lastID += 1;
    patient.id = lastID;
    patient.active = true;
    patients.push(patient);
    return patient;
}

function removePatient(id) {
    patients.forEach(patient => {
        if (patient.id === id) {
            patient.active = false;
        }
    });
}

function findPatient(id) {
    let result = null;
    patients.forEach(patient => {
        if (patient.id === id && patient.active) {
            result = patient;
        }
    });
    return result;
}

function getPatients() {
    const result = patients.filter(p => p.active).slice();
    result.sort((a, b) => a.start > b.start);
    return result;
}

function getEnd(patient) {
    return (new Date((new Date(patient.start)).getTime() + patient.duration * 60000)).toISOString();
}

function insertPatient(patient) {
    const {doctor, priority} = patient;
    const doctorPatients = getPatients().filter(p => p.doctor === doctor && p.priority <= priority);
    const lastPatient = (doctorPatients.length > 0)
        ? doctorPatients.reduce((prev, current) => (getEnd(prev) > getEnd(current)) ? prev : current)
        : null;
    const start = lastPatient ? getEnd(lastPatient) : (new Date()).toISOString();
    const queuePrefix = `${priority}${doctors[doctor].queueNumber}`;
    const queueNumber = lastPatient ? `${queuePrefix}${("00" + (+lastPatient.queueNumber.slice(2) + 1)).slice(-2)}` : `${queuePrefix}01`;
    const result = addPatient(Object.assign(patient, {start, queueNumber}));

    const patientsToMove = getPatients().filter(p => p.doctor === doctor && p.priority > priority);
    patientsToMove.forEach(p => p.start = getEnd(result));

    return result;
}

function updateClients() {
    wss.clients.forEach(client => {
        client.send(JSON.stringify({type: 'new_patient', patients: getPatients()}));
    });
}

api.post('/patients', (req, res, next) => {
    const {name, age, ssn, phone, address, symptoms, priority, duration, doctor} = req.body;
    const patient = insertPatient(req.body);
    updateClients();
    const {id, queueNumber} = patient;
    res.json({id, queue_number: queueNumber});
});

api.post('/patients/receive', (req, res, next) => {
    const {id, doctor} = req.body;
    console.log(req.body);
    const patient = findPatient(id);
    patient.room = doctors[doctor].room;
    updateClients();
    res.json({result: "ok"});
});

api.post('/patients/done', (req, res, next) => {
    const {id} = req.body;
    removePatient(id);
    updateClients();
    res.json({result: "ok"});
});

api.get('/patients', async (req, res, next) => {
    const patients = getPatients();
    res.json(patients);
});

api.ws('/', (ws, req) => {
});

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(express.json());
app.use('/api/v1', api);
app.listen(port);
console.log('server started on ' + port);
