-- Up
CREATE TABLE patients (
    id INTEGER PRIMARY KEY,
    name TEXT,
    ssn TEXT,
    phone TEXT,
    address TEXT,
    symptoms TEXT,
    priority TEXT,
    start TEXT,
    duration INTEGER,
    queue_number TEXT);

-- Down
DROP TABLE patients;
